<?php
/**
 * @file uc_field_class_attribute.inc
 * Node functions for uc_field_class_attribute
 *
 * (C)2011 Michael Moritz miiimooo/at/drupal.org http://drupal.org/user/62954
 * (C)2013 Krivchun Igor igor/at/krivchun.ru http://ikrivchun.moikrug.ru
 */

/**
 * Synchronize product field values with product attribue values
 * 
 * @TODO: must be fully refactored!
 * 
 * @param $node
 *   Node object.
 * @param $mode
 *   Action type: insert, update or delete.
 */
function _uc_field_class_attribute_nodeapi($node, $mode = NULL) {
  if (!_uc_field_class_attribute_type_valid($node->type)) {
    return;
  }
  $attribute_settings = uc_field_class_attribute_load_settings($node->type);
  foreach($attribute_settings->fields as $fiid => $enabled) {
    if (!$enabled) {
      continue;
    }
    $bundle = field_extract_bundle('node', $node);
    $instances = field_info_instances('node', $bundle);
    $found = false;
    foreach($instances as $field_name => $instance) {
      if ($instance['id'] == $fiid) {
        $found = true;
        break;
      }
    }
    if (!$found) {
      continue; //TODO warning?
    }
    $instance = (object) $instance;
    $attribute = uc_field_class_attribute_load($node->type, $instance->id);
    if (!$attribute) {
//       dsm($instance);
      $attribute = $instance;
      $attribute->display = 1;
      $attribute->name = $instance->field_name;
      $attribute->options = array();
      uc_attribute_save($attribute);
      $attribute = (object) $attribute;
      $field_attribute = array(
        "aid" => $attribute->aid,
        "type" => $node->type,
        "field_instance_id" => $instance->id,
      );
      drupal_write_record('uc_field_class_attribute_node', $field_attribute);
//       $attribute = (object) $field_attribute;
    }
    $product_attribute = (object) $attribute;
//     dsm($product_attribute);
    $attributes = uc_product_get_attributes($node->nid);
    // UPDATE or INSERT
    $exists = isset($attributes[$product_attribute->aid]);
//     $primary_keys = ($is_new ? NULL : array('aid', 'nid'));
    $product_attribute->nid = $node->nid;
    /// NOTE no API for this?
    if (!$exists) {
      drupal_write_record('uc_product_attributes', $product_attribute);
      $product_attribute = (object) $product_attribute;
    }

    $items = field_get_items('node', $node, $field_name);
    $allowed_values = uc_field_class_attribute_field_values($instance->field_id);
    $product_active_attribute_options = uc_field_class_attribute_product_active_options($node->nid, $product_attribute->aid);

    foreach($items as $index => $item) {
      // this returns markup in an array
      // could probably make the $display configurable
      $value = field_view_value('node', $node, $field_name, $item);
      $value = strip_tags($value['#markup']);
      $product_attribute_oid = 0;
      foreach($product_attribute->options as $oid => $option) {
        if ($option->name == $value) {
          $product_attribute_oid = $oid;
          unset($product_attribute->options[$oid]);
          break;
        }
      }
      if (!$product_attribute_oid) {
        ///TODO could this already exist - should probably check through the table first
        $option = array(
          "aid" => $product_attribute->aid,
          'name' => $value,
          'ordering' => array_search($value, array_keys($allowed_values)),
        );
        $option = (object) $option;
        uc_attribute_option_save($option);
        $option = (object) $option;
        $option->nid = $node->nid;
        drupal_write_record('uc_product_options', $option);
      } else if( !in_array($option->oid, $product_active_attribute_options) ) {
          $option->nid = $node->nid;
          $option->ordering = array_search($option->name, array_keys($allowed_values));
          drupal_write_record('uc_product_options', $option);
      }
    }
    //at this point $product_attribute->options holds all options that have "disappeared"
    //from the Field API field
    foreach($product_attribute->options as $oid => $option) {
      db_delete('uc_product_options')
        ->condition('oid', $oid)
        ->condition('nid', $node->nid)
        ->execute();
      if( !in_array($option->name, $allowed_values) ) {//delete attribute value only if field option is deleted
        db_delete('uc_attribute_options')
          ->condition('oid', $oid)
          ->execute();
      }
    }
  }
}

/**
 * Load uc attribute, mapped for specified field for specified node type.
 * 
 * @param type $node_type
 *   Node type.
 * @param type $fiid
 *   Mapped field id.
 * 
 * @return
 *   Uc attribute object.
 */
function uc_field_class_attribute_load($node_type, $fiid) {
  $aid = db_query("SELECT aid FROM {uc_field_class_attribute_node}
    WHERE type = :node_type AND field_instance_id = :fid",
    array(':node_type' => $node_type, ':fid' => $fiid))->fetchField();
  if (!$aid) {
    return false;
  }
  return uc_attribute_load($aid);
}

/**
 * Load list of avalaible values for specified field.
 * 
 * @param type $field_id
 *   Field id.
 * 
 * @return
 *   Array of all allowed field values.
 */
function uc_field_class_attribute_field_values($field_id) {
   $result = db_query("SELECT data FROM {field_config} WHERE id = :id", array(':id' => $field_id));
   if( !$result->rowCount() ) {
       return false;
   }

   //Fetch data from first column from next row and unserialize
   $fieldData = unserialize($result->fetchColumn());
   return $fieldData['settings']['allowed_values'];
}

/**
 * Load all active attribute options for specified product
 * 
 * @param type $nid
 *   Product node id.
 * @param type $aid
 *   Attribute id.
 * 
 * @return
 *   Array of active attribute options.
 */
function uc_field_class_attribute_product_active_options($nid, $aid) {
    $result = db_query("SELECT po.oid FROM {uc_product_options} po LEFT JOIN {uc_attribute_options} ao ON po.oid = ao.oid WHERE po.nid = :nid AND ao.aid = :aid", array(':nid' => $nid, ':aid' => $aid));
    if( !$result->rowCount() ) {
       return array();
   }

   return $result->fetchCol();
}

/**
 * Remove product field to attribute mapping and appropriate uc attribute.
 * 
 * @param $field_id
 *   Unmapped field id.
 * @param type $node_type
 *   Unmapped node type.
 */
function _uc_field_class_attribute_node_delete($field_id, $node_type) {
  $result = db_query("SELECT aid FROM {uc_field_class_attribute_node}
    WHERE type = :node_type AND field_instance_id = :field_id",
    array(':node_type' => $node_type, ':field_id' => $field_id));
  if( !$result->rowCount() ) {
      return;
  }

  foreach($result as $row) {
    db_query("DELETE po FROM {uc_product_options} po LEFT JOIN {uc_attribute_options} ao ON po.oid = ao.oid 
        WHERE ao.aid = :aid", array(':aid' => $row->aid));
    db_delete('uc_attribute_options')
      ->condition('aid', $row->aid)
      ->execute();
    db_delete('uc_attributes')
      ->condition('aid', $row->aid)
      ->execute();
  }
  db_delete('uc_field_class_attribute_node')
    ->condition('type', $node_type)
    ->condition('field_instance_id', $field_id)
    ->execute();
}